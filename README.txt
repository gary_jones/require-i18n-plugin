Pre-requisites
--------------

You need to install the GPG binaries for your environment before building.

The GPG executable needs to be on your path at build time.

E.g. For Mac OSX get the binsaries from here https://gpgtools.org/
You will need to gernerate a new key to be added to your keychain using these tools.

The first build will prompt you for the password for this key. After that once it is added to your keychain you should be able to build the project unchallenged.
