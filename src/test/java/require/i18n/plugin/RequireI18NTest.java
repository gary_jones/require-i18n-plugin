package require.i18n.plugin;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.RegexFileFilter;
import org.apache.maven.plugin.MojoExecutionException;
import org.hamcrest.collection.IsArrayWithSize;
import org.hamcrest.core.Is;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;


public class RequireI18NTest {
	
	@Rule
	public TemporaryFolder propertiesFolder = new TemporaryFolder();

	@Rule
	public TemporaryFolder requirejsFolder = new TemporaryFolder();
	private File rootNlsDir;
	
	// Properties files names
	private static final String empty_default[][] = {};
	private static final String colors_default[][] = {{"red","red"},{"green","green"},{"blue","blue"}};
	private static final String colors_fr[][] = {{"red","redfr"},{"green","greenfr"},{"blue","bluefr"}};
	private static final String colors_fr_FR[][] = {{"red","redfrfr"},{"green","greenfrfr"},{"blue","bluefrfr"}};
	
	// Properties to add
	private static final Map<String, String[][]> propertiesValues = new HashMap<String, String[][]>(){
        {
        	put("empty", empty_default);
            put("colors", colors_default);
            put("colors_fr", colors_fr);
            put("colors_fr_FR", colors_fr_FR);
        }
    };
	
	// Properties files created for tests
	private List<File> propertiesFiles = new ArrayList<File>();

	private RequireI18N requireI18N;

    @Before
    public void setUp() throws IOException {
    	requireI18N = new RequireI18N();
    	requireI18N.setProjectBaseDirectory(propertiesFolder.getRoot());
    	requireI18N.setPropertiesDir(propertiesFolder.getRoot());
    	requireI18N.setRequireI18NDir(requirejsFolder.newFolder("requirejs"));
    	
    	rootNlsDir = new File(requirejsFolder.getRoot().getAbsolutePath() + File.separator + "requirejs" + File.separator + "nls" + File.separator + "root");
    }
    
    @After
    public void cleanUp() {
    	requireI18N = null;
    }

    @Test
    public void shouldCreateASingleFileContainingNoAttributesForAnEmptyPropertyFile() throws IOException, MojoExecutionException {
    	File expectedFile = createPropertyFile("empty");
    	requireI18N.execute();
    	assertThat(expectedFile.exists(), is(true));
    	assertThat(expectedFile.length(), is(0l));
    	assertThat( rootNlsDir.listFiles().length, is(1));
    	assertThat( rootNlsDir.listFiles()[0].getName(), is("empty.js"));
    }

	@Test
	public void shouldCreateNoFilesIfNoPropertiesFilesExist() throws MojoExecutionException {
		requireI18N.execute();
		assertThat( rootNlsDir.list().length, is(0));
	}
	
    @Test
    public void shouldCreateOneFileForAValidPropertyFile() throws IOException, MojoExecutionException {
    	File expectedFile = createPropertyFile("colors");
    	requireI18N.execute();
    	
    	assertThat( rootNlsDir.listFiles().length, is(1));
    	assertThat( rootNlsDir.listFiles()[0].getName(), is("colors.js"));
    	
    }

    @Test
    public void shouldCreateARequireFileForEachPropertiesFile() throws IOException, MojoExecutionException {
    	final int expectedNumberOfJSFiles = 6;
    	createAllPropertiesFiles();
    	requireI18N.execute();
    	Collection<File> requireFiles = FileUtils.listFiles(
    			  rootNlsDir.getParentFile(), 
    			  new RegexFileFilter("^(.*?)"), 
    			  DirectoryFileFilter.DIRECTORY
    			);
    	assertThat( requireFiles.size(), is(expectedNumberOfJSFiles));
    }

	private File createPropertyFile(String key) throws IOException {
		File newPropFile = propertiesFolder.newFile(key+".properties");
		BufferedWriter out = new BufferedWriter(new FileWriter(newPropFile));
        
		String [][] properties = propertiesValues.get(key);
		for (int i=0; i<properties.length; i++) {
			out.write(properties[i][0] + "=" + properties[i][1]);
			out.newLine();
		}
		out.close();
		
		return newPropFile;
	}
	
	private void createAllPropertiesFiles() throws IOException {
    	Iterator<String> propFiles = propertiesValues.keySet().iterator();
    	while (propFiles.hasNext()) {
    		propertiesFiles.add(createPropertyFile(propFiles.next()));
    	}
	}
}
