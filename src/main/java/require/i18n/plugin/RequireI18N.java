package require.i18n.plugin;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

/** 
 * Require.js I18N module generator
 *
 * @author Gary Jones 
 * 
 * Goal which generates the require.js i18n modules from java properties files
 */
@Mojo(name = "generate", defaultPhase = LifecyclePhase.PROCESS_SOURCES)
public class RequireI18N extends AbstractMojo {

	/**
	 * File name constants
	 */
	private static final String EXTENSION_SEPERATOR = ".";
	private static final String JAVA_I18N_SEPERATOR = "_";
	private static final String REQUIRE_I18N_SEPERATOR = "-";
	private static final String JS_EXT = ".js";
	
	/**
	 * Require.js i18n module constants
	 */
	private static final String KEY_VALUE_SEPERATOR = ": ";
	private static final String VALUE_SEPERATOR = ",";
	private static final String QUOTE = "\"";
	private static final String PADDING = "    ";
	private static final String DEFINE_START = "define({";
	private static final String DEFINE_END = "});";
	
	/**
	 * Contains the list of root modules that need to have the root elements
	 */
	private final HashMap<String, String> rootModules = new HashMap<String, String>();
	
	/**
	 * Contains a map of all require.js files mapped to key value pairs to write to that file
	 * 
	 *  key : file name to create
	 *  value : Map<String, String> key, values
	 */
	private final Map<String, Map<String,String>> requireI18NFileToValuesMap = new HashMap<String, Map<String,String>>();

	/**
	 * Location of the project
	 */
	@Parameter(defaultValue = "${project.basedir}", property = "projectRoot", required = true)
	private File projectBaseDirectory;

	/**
	 * Location of the java properties
	 */
	@Parameter(defaultValue = "${project.basedir}/src/main/resources/", property = "propertiesDir", required = true)
	private File propertiesDir;

	/**
	 * Location of the generated require i18n js files
	 */
	@Parameter(defaultValue = "${project.basedir}/src/main/webapp/resources/js", property = "requireI18NDir", required = true)
	private File requireI18NDir;

	public void execute() throws MojoExecutionException {
		getLog().info("Project directory " + projectBaseDirectory);
		getLog().info("Project properties directory " + propertiesDir);
		getLog().info("Generating require.js i18n resources into " + requireI18NDir);

		/**
		 * Discover properties files
		 */
		final String [] propertiesExtension = {"properties"};
		final File[] propertiesFiles = FileUtils.listFiles(propertiesDir,propertiesExtension,true).toArray(new File[0]);
		getLog().info("Found " + propertiesFiles.length + " properties files");

		/**
		 * Require generated files
		 */
		final File generatedDirectory = new File(requireI18NDir.getAbsolutePath() + File.separator + "nls");
		getLog().info("Creating directory for resources " + generatedDirectory);

		final File rootDirectory = createRequireDirectories(generatedDirectory);

		try {
			for (int i = 0; i < propertiesFiles.length; i++) {
				getLog().debug("property file " + propertiesFiles[i]);
				String filename = propertiesFiles[i].getName().toLowerCase();
				boolean isRootModule = true;
				String rootName = "root";
				File rootDir = rootDirectory;

				if (!isRootModule(filename)) {
					isRootModule = false;
					String directoryName = filename.substring(filename.indexOf(JAVA_I18N_SEPERATOR) + 1, filename.indexOf(EXTENSION_SEPERATOR)).toLowerCase();
					directoryName = directoryName.replace(JAVA_I18N_SEPERATOR, REQUIRE_I18N_SEPERATOR);
					File directoryForFile = new File(generatedDirectory.getAbsolutePath() + File.separator + directoryName);
					if (!FileUtils.directoryContains(generatedDirectory, new File(directoryName))) {
						directoryForFile.mkdir();
					}

					rootName = directoryName;
					rootDir = directoryForFile;
				}
				savePropertiesToRequireModuleHash(propertiesFiles[i], rootDir, isRootModule);
				createFileRootKey(propertiesFiles[i], generatedDirectory, rootName, isRootModule);
			}

			writeRootModules();
			writeModules();
		} catch (Exception e1) {
			getLog().info("Did not generate require files correctly " + generatedDirectory + "[" + e1 + "]");
		}

	}

	private void writeModules() {
		Iterator<String> requireFileNames = requireI18NFileToValuesMap.keySet().iterator();
		while(requireFileNames.hasNext()) {
			String nextRequireFileName = requireFileNames.next();
			copyPropertiesToRequireModule(new File(nextRequireFileName),requireI18NFileToValuesMap.get(nextRequireFileName));
		}
	}

	private void savePropertiesToRequireModuleHash(File file, File directoryForFile, boolean isRootModule) {
		String requireI18NFileToCreate = directoryForFile.getAbsolutePath() + File.separator + getBaseFileName(file, !isRootModule) + JS_EXT;
		Map<String, String> newKeyValues = loadProperties(file);
		if (requireI18NFileToValuesMap.containsKey(requireI18NFileToCreate)) {
			Map<String,String> existingKeyValues = requireI18NFileToValuesMap.get(requireI18NFileToCreate);
			
			Iterator<String> keys = newKeyValues.keySet().iterator();
			while(keys.hasNext()) {
				final String key = keys.next();
				existingKeyValues.put(key, newKeyValues.get(key));
			}
			
		} else {
			requireI18NFileToValuesMap.put(requireI18NFileToCreate, newKeyValues);
		}
			
	}

	private void createFileRootKey(File propertiesFile, final File generatedDirectory, String directoryName, boolean isRootModule) {
		String fileKey = generatedDirectory.getAbsolutePath() + File.separator + getBaseFileName(propertiesFile, !isRootModule) + JS_EXT;
		String value = rootModules.get(fileKey);
		if (value != null) {
			value += VALUE_SEPERATOR + directoryName;
		} else {
			value = directoryName;
		}
		rootModules.put(fileKey, value);

		getLog().debug("Create file " + getBaseFile(propertiesFile, !isRootModule) + " in directory " + generatedDirectory);
	}

	private boolean isRootModule(String filename) {
		return !filename.contains(JAVA_I18N_SEPERATOR);
	}

	private File createRequireDirectories(final File generatedDirectory) {
		File rootDirectory = null;
		try {
			getLog().info("Cleaning up old resources...");
			FileUtils.deleteDirectory(generatedDirectory);
			final boolean hasDirectoryBeenCreated = generatedDirectory.mkdirs();
			getLog().info("Created properly : " + hasDirectoryBeenCreated);
			rootDirectory = new File(generatedDirectory.getAbsolutePath() + File.separator + "root");
			rootDirectory.mkdir();

		} catch (Exception e1) {
			getLog().info("Did not fully clean up " + generatedDirectory);
		}

		return rootDirectory;
	}

	private void copyPropertiesToRequireModule(File requireModule, Map<String,String> values) {
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(requireModule,true));
			writeRequireModule(values, writer);
			writer.flush();
			writer.close();
		} catch (Exception e) {
			getLog().debug("Failed to create require module " + requireModule);
		}
	}

	private void writeRootModules() throws IOException {

		Iterator<String> iter = rootModules.keySet().iterator();

		while (iter.hasNext()) {
			String key = iter.next();
			String value = rootModules.get(key);
			List<String> rootKeyList = new ArrayList<String>(Arrays.asList(value.split(VALUE_SEPERATOR)));
			String [] rootKeyListDeduped = new HashSet<String>(rootKeyList).toArray(new String [0]);

			getLog().info("ROOT " + "key=" + key + " value=" + value);

			try {
				BufferedWriter writer = new BufferedWriter(new FileWriter(key,true));
				
				writer.write(DEFINE_START);
				writer.newLine();

				for (int i=0;i<rootKeyListDeduped.length;i++) {
					writer.write(PADDING + QUOTE + rootKeyListDeduped[i] + QUOTE + KEY_VALUE_SEPERATOR + true);
					if (i+1<rootKeyListDeduped.length) {
						writer.write(VALUE_SEPERATOR);
					}
					writer.newLine();
				}

				writer.write(DEFINE_END);
				writer.newLine();

				writer.flush();
				writer.close();
			} catch (Exception e) {
				getLog().debug("Failed to create require module " + key);
			}

		}
	}

	private void writeRequireModule(Map<String, String> values, BufferedWriter writer) throws IOException {
		writer.write(DEFINE_START);
		writer.newLine();

		Iterator<String> iter = values.keySet().iterator();
		while (iter.hasNext()) {
			String key = iter.next();
			String value = values.get(key);

			String str = PADDING + QUOTE + key + QUOTE + KEY_VALUE_SEPERATOR + QUOTE + value + QUOTE;
			getLog().debug(str);
			writer.write(str);
			if (iter.hasNext()) {
				writer.write(VALUE_SEPERATOR);
			}
			writer.newLine();
		}
		String endDefine = DEFINE_END;
		writer.write(endDefine);
		writer.newLine();
	}

	private Map<String, String> loadProperties(File propertiesFile) {
		Map<String, String> values = new HashMap<String, String>();
		getLog().info("Loading properties for " + propertiesFile);
		try {
			Properties prop = new Properties();
			FileInputStream in = new FileInputStream(propertiesFile);
			prop.load(in);
			Enumeration<?> keys = prop.propertyNames();
			while (keys.hasMoreElements()) {
				String key = (String) keys.nextElement();
				values.put(key, (String) prop.get(key));
			}
			in.close();
		} catch (Exception e) {
			getLog().debug("Failed to load properties from " + propertiesFile);
		}
		return values;
	}

	private File getBaseFile(File file, boolean isLocalised) {
		return new File(getBaseFileName(file, isLocalised));
	}

	private String getBaseFileName(File file, boolean isLocalised) {
		if (isLocalised) {
			return file.getName().toLowerCase().substring(0, file.getName().indexOf(JAVA_I18N_SEPERATOR));
		} else {
			return file.getName().toLowerCase().substring(0, file.getName().indexOf(EXTENSION_SEPERATOR));
		}
	}

	public File getProjectBaseDirectory() {
		return projectBaseDirectory;
	}

	public void setProjectBaseDirectory(File projectBaseDirectory) {
		this.projectBaseDirectory = projectBaseDirectory;
	}

	public File getPropertiesDir() {
		return propertiesDir;
	}

	public void setPropertiesDir(File propertiesDir) {
		this.propertiesDir = propertiesDir;
	}

	public File getRequireI18NDir() {
		return requireI18NDir;
	}

	public void setRequireI18NDir(File requireI18NDir) {
		this.requireI18NDir = requireI18NDir;
	}

}
